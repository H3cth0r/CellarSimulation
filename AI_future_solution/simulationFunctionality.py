
import neat
import pygame
import sys
import os
from main import SCREEN, CROSSROADS
from Robot import Robot
import random



def remove(index):
    cars.pop(index)
    ge.pop(index)
    nets.pop(index)

generationNumber = 0


def eval_genomes(genomes, config):
    global cars, ge, nets

    itearation_since_last_dead = 0
    cars = []
    ge = []
    nets = []

    global generationNumber
    print(f"gen num {generationNumber}")


    possible_launch_robot = [(330, 330), (630, 330), (600, 365), (180, 365), (1000, 600), (900, 150), (700, 350), (700, 310), (600, 150), (150, 600)]
    pos_coord = random.choice(possible_launch_robot)
    for genome_id, genome in genomes:
        cars.append(pygame.sprite.GroupSingle(Robot(pos_coords=pos_coord, curr_generation=generationNumber)))
        ge.append(genome)
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        nets.append(net)
        genome.fitness = 0

    generationNumber += 1

    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        SCREEN.blit(CROSSROADS, (0, 0))
        

        if len(cars) == 0:
            break

        if itearation_since_last_dead >= 1500:
            for i, car in enumerate(cars):
                ge[i].fitness += 1
                remove(i)
            break

        for i, car in enumerate(cars):
            ge[i].fitness += 1
            if not car.sprite.alive:
                remove(i)
                itearation_since_last_dead = 0
        

        for i, car in enumerate(cars):
            output = nets[i].activate(car.sprite.data())
            if output[0] > 0.7:
                car.sprite.direction = 1
            if output[1] > 0.7:
                car.sprite.direction = -1
            if output[0] <= 0.7 and output[1] <= 0.7:
                car.sprite.direction = 0

        # Update
        for car in cars:
            car.draw(SCREEN)
            car.update()

        itearation_since_last_dead += 1
        pygame.display.update()


# Setup NEAT Neural Network
def run(config_path):
    global pop
    config = neat.config.Config(
        neat.DefaultGenome,
        neat.DefaultReproduction,
        neat.DefaultSpeciesSet,
        neat.DefaultStagnation,
        config_path
    )

    pop = neat.Population(config)

    pop.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)

    pop.run(eval_genomes, 120)
