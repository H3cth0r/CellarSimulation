
import os
import pygame
import math
from main import SCREEN
import random

# (330, 330), (630, 330), (600, 365), (180, 365), (1000, 600), (900, 150), (700, 350), (700, 310), (600, 150), (150, 600)
class Robot(pygame.sprite.Sprite):
    random_location = [(330, 330), (630, 330), (600, 365), (180, 365), (1000, 600), (900, 150), (700, 350), (700, 310), (600, 150), (150, 600)]
    stepCounter = 0
    changedCharacter = False
    def __init__(self, pos_coords, curr_generation):
        super().__init__()

		# Set image to the character car
        self.original_image 	= pygame.image.load(os.path.join("Assets", "robot_001.png"))
        self.current_gen = curr_generation
        self.image				= self.original_image
        self.rect				= self.image.get_rect(center=pos_coords)

		# Setting velocity vector, rotation velocity
        self.velocity_vector 	= pygame.math.Vector2(0.5, 0)
        self.rotation_angle 	= 0
        self.rotation_velocity 	= 5
        self.direction 			= 0
        self.alive 				= True

		# Creating list of radars
        self.radars 			= []

    def update(self):

		# Clear and reset the radars
        self.radars.clear()
		
		# Applying running vector
        self.drive()
        self.rotate()

		# Set new radars methods
        for radar_angle in (-60, -30, 0, 30, 60):
            self.radar(radar_angle, 120)
        self.radar(180, 120)
		
		# Method for when collision and scanning with vectors
        self.collision()

		# Apply data collection
        self.data()

        """
        if self.stepCounter >= 200 and self.current_gen >= 2 and self.changedCharacter == False:
            self.changedCharacter = True
            self.original_image = pygame.image.load(os.path.join("Assets", "robot_002.png"))
            self.image = self.original_image
            self.rect = self.image.get_rect(center=self.rect.center)
        """
        self.stepCounter += 1

	# Method for making the car keep moving forward
    def drive(self):
        self.rect.center 		+= self.velocity_vector * 6

	# Method for handdling collisions
    def collision(self):
        length = 12
        collision_point_right 	= [int((self.rect.center[0]) + math.cos(math.radians(self.rotation_angle + 44)) * length),
                                   int((self.rect.center[1]) - math.sin(math.radians(self.rotation_angle + 44)) * length)]
        collision_point_left 	= [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle - 44)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle - 44)) * length)]

        collision_point_ll 	    = [int(self.rect.center[0] - math.cos(math.radians(self.rotation_angle)) * length),
                                   int(self.rect.center[1] + math.sin(math.radians(self.rotation_angle)) * length)]
        collision_point_rr 	    = [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle)) * length)]
        
        collision_point_uu 	    = [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle + 90)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle + 90)) * length)]
        collision_point_dd 	    = [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle - 90)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle - 90)) * length)]

        collision_point_lb 	    = [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle + 135)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle + 135)) * length)]
        collision_point_rb 	    = [int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle - 135)) * length),
                                   int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle - 135)) * length)] 

        # Die on Collision
        if SCREEN.get_at(collision_point_right) == pygame.Color(102, 102, 102, 255) \
            or SCREEN.get_at(collision_point_left) == pygame.Color(102, 102, 102, 255)\
                 or SCREEN.get_at(collision_point_ll) == pygame.Color(102, 102, 102, 255)\
                    or SCREEN.get_at(collision_point_rr) == pygame.Color(102, 102, 102, 255) \
                        or SCREEN.get_at(collision_point_uu) == pygame.Color(102, 102, 102, 255) \
                            or SCREEN.get_at(collision_point_dd) == pygame.Color(102, 102, 102, 255) \
                                or SCREEN.get_at(collision_point_lb) == pygame.Color(102, 102, 102, 255)\
                                    or SCREEN.get_at(collision_point_rb) == pygame.Color(102, 102, 102, 255):
            self.alive = False
        
        """
        if SCREEN.get_at(collision_point_right) == pygame.Color(102, 102, 102, 255) \
                or SCREEN.get_at(collision_point_left) == pygame.Color(102, 102, 102, 255):
            self.alive 			= False
        """
        # Draw Collision Points
        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_ll, 4)
        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_rr, 4)

        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_uu, 4)
        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_dd, 4)

        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_rb, 4)
        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_lb, 4)

        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_right, 4)
        #pygame.draw.circle(SCREEN, (0, 255, 255, 0), collision_point_left, 4)

	# Method for rotating the car
    def rotate(self):
        if self.direction == 1:
            self.rotation_angle -= self.rotation_velocity
            self.velocity_vector.rotate_ip(self.rotation_velocity)
        if self.direction == -1:
            self.rotation_angle += self.rotation_velocity
            self.velocity_vector.rotate_ip(-self.rotation_velocity)

        self.image = pygame.transform.rotozoom(self.original_image, self.rotation_angle, 0.1)
        self.rect = self.image.get_rect(center=self.rect.center)

	# Method for handling rada scan
    def radar(self, radar_angle, max_distance):
        length				= 0
        x					= int(self.rect.center[0])
        y					= int(self.rect.center[1])
		
        while not SCREEN.get_at((x, y)) == pygame.Color(102,102,102,255) and length < max_distance:
            length			+= 1
            x				= int(self.rect.center[0] + math.cos(math.radians(self.rotation_angle + radar_angle)) * length)
            y				= int(self.rect.center[1] - math.sin(math.radians(self.rotation_angle + radar_angle)) * length)

        # Draw Radar
        #pygame.draw.line(SCREEN, (255, 255, 255, 255), self.rect.center, (x, y), 1)
        #pygame.draw.circle(SCREEN, (0, 255, 0, 0), (x, y), 3)

        dist				= int(math.sqrt(math.pow(self.rect.center[0] - x, 2) + math.pow(self.rect.center[1] - y, 2)))

        self.radars.append([radar_angle, dist])

    def data(self):
        input				= [0, 0, 0, 0, 0, 0]
        for i, radar in enumerate(self.radars):
            input[i]		= int(radar[1])
        return input
