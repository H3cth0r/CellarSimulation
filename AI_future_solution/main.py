# system modules
import os

# Main Functional libraries
import pygame

# Importing functional classes
from simulationFunctionality import *

# Pygame screen size constants
S_WIDTH		=   1200
S_HEIGHT	=	800
SCREEN      =   pygame.display.set_mode((S_WIDTH, S_HEIGHT))

# Setting the image on the screen
CROSSROADS	=	pygame.image.load(os.path.join("Assets", "mapaBodega.png"))

# setting obstacle
#OBSTACLE = pygame.image.load(os.path.join("Assets", "obs_01.png"))
#OBSTACLE = pygame.transform.scale(OBSTACLE, (30, 10))

if __name__ == '__main__':
    local_dir        = os.path.dirname(__file__)
    config_file_path = os.path.join(local_dir, "config.txt")

    run(config_file_path)
